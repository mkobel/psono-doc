---
title: Throttle Rates
metaTitle: Throttle Rates Settings | Psono Documentation
meta:
  - name: description
    content: Configuration of custom throttle rates to adjust rate limits.
---


# Throttle Rates

## Preamble

This guide will explain how to configure custom throttle rates for custom rate limits. Psono comes preconfigured with
reasonable defaults that are chosen to be produce the least friction and false positive blocks, yet sometimes you may
run into an Error 429 and may want to customize these rates.

::: tip
Rates are "global" (if you use a central cache) or per process (controlled by the environment variable `UWSGI_PROCESSES`
and defaults to 10)
:::

## Rate Limit Settings

### Access without credentials

All access without credentials is controlled by this rate limit. This rate limit works on a 'per IP' basis.

```yaml
THROTTLE_RATE_ANON: '1440/day'
```

Restart the server afterwards


### Login

All login requests are throttled by scope. This rate limit works on a 'per IP' basis.

```yaml
THROTTLE_RATE_ANON: '1440/day'
```

Restart the server afterwards

### Link Share Access

All requests to access a link share are controlled by this rate limit. This rate limit works on a 'per IP' basis.

```yaml
THROTTLE_RATE_LINK_SHARE_SECRET: '60/hour'
```

Restart the server afterwards

### Emergency Code and Recovery Code Access

All requests to use emergency codes and recovery codes is controlled by this rate limit. This rate limit works on a 'per IP' basis.

```yaml
THROTTLE_RATE_PASSWORD: '24/day'
```

Restart the server afterwards

### All authenticated requests

All requests that are authenticated and not controlled by one of the other rate limits use this rate limit. This rate limit works on a 'per user' basis.

```yaml
THROTTLE_RATE_USER: '86400/day'
```

Restart the server afterwards

### All authenticated requests

All requests to the `/healthcheck/` endpoint are controlled by this rate limit. This rate limit works on a 'per IP' basis.

```yaml
THROTTLE_RATE_HEALTH_CHECK: '61/hour'
```

Restart the server afterwards

### All user status checks

All clients periodically ask the server for updates like new shares and so on. The client is calling the `/user/status/`
endpoint and this config controls the rate limit for this endpoint. This rate limit works on a 'per user' basis.

```yaml
THROTTLE_RATE_STATUS_CHECK: '61/hour'
```

Restart the server afterwards

### Google Authenticator

Whenever a user uses a google authenticator as second factor then this config will control the allowed rate limit. This rate limit works on a 'per user' basis.

```yaml
THROTTLE_RATE_GA_VERIFY: '6/minute'
```

Restart the server afterwards

### DUO

Whenever a user uses DUO as second factor then this config will control the allowed rate limit. This rate limit works on a 'per user' basis.

```yaml
THROTTLE_RATE_DUO_VERIFY: '6/minute'
```

Restart the server afterwards

### Yubikey

Whenever a user uses a Yubikey as second factor then this config will control the allowed rate limit. This rate limit works on a 'per user' basis.

```yaml
THROTTLE_RATE_YUBIKEY_OTP_VERIFY: '6/minute'
```

Restart the server afterwards

### Registration

Whenever a user registers then this config will control the allowed rate limit. This rate limit works on a 'per IP' basis.

```yaml
THROTTLE_RATE_REGISTRATION: '6/minute'
```

Restart the server afterwards

### User deletion

Whenever a user wants to delete his account he needs to provide his password. This rate limit will protect against bruteforce attacks. This rate limit works on a 'per user' basis.

```yaml
THROTTLE_RATE_USER_DELETE: '6/minute'
```

Restart the server afterwards

### User update

Whenever a user wants to update his password or email address he needs to provide his (old) password. This rate limit will protect against bruteforce attacks. This rate limit works on a 'per user' basis.

```yaml
THROTTLE_RATE_USER_UPDATE: '6/minute'
```

Restart the server afterwards

### Fileserver Alive

The fileserver is periodically announcing it being alive and this setting controls how often a single ip can call the appropriate endpoint. This rate limit works on a 'per ip' basis.

```yaml
THROTTLE_RATE_FILESERVER_ALIVE: '6/minute'
```

Restart the server afterwards

### Fileserver Upload

Whenever a user is uploading something to the fileserver, the fileserver will relay the request to the server. This rate
limit controls how often such a request can be handled. This rate limit works on a 'per fileserver' basis.

```yaml
THROTTLE_RATE_FILESERVER_UPLOAD: '10000/minute'
```

Restart the server afterwards

### Fileserver Download

Whenever a user is downloading something from the fileserver, the fileserver will relay the request to the server. This rate
limit controls how often such a request can be handled. This rate limit works on a 'per fileserver' basis.

```yaml
THROTTLE_RATE_RATE_FILESERVER_DOWNLOAD: '10000/minute'
```

Restart the server afterwards
