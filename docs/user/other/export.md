---
title: Export Passwords
metaTitle: Export Passwords | Psono Documentation
meta:
  - name: description
    content: How to export passwords.
---

# Export Passwords

## Preamble

Psono provides the possibility to export secrets and passwords.

::: warning
Files are currently not part of the export.
:::

## Export


1)  Go to `Other`:

![Step 1 Go to Other](/images/user/export/step1-go-to-other.jpg)

2)  Click `Export`:

![Step 2 click Export](/images/user/export/step2-export.jpg)

You have now a file with all your passwords. You can open it in any text editor and import it in any other Psono instance.
