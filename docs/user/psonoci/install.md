---
title: Install psonoci
metaTitle: Install psonoci | Psono Documentation
meta:
  - name: description
    content: Install instruction for psonoci, the psono CI / CD integration utility
---

# Install psonoci (Beta)

Install instructions for psonoci.


## Preamble

At this point we assume that you already have a psono server running, ready to accept connections, have it filled with 
secrets and want to integrate Psono now into your build pipeline to secure your secrets with a second layer of defense.
We further assume that you have a restricted API key configured. If not follow the [guide to create an API key](/user/api-key/creation.html).

## Installation

psonoci comes in different flavors:

| Architecture | OS Family | Path |
|--------------|-----------|------|
| x86_64 | Linux | https://get.psono.com/psono/psono-ci/x86_64-linux/psonoci |
| ARMv7 | Linux | https://get.psono.com/psono/psono-ci/armv7-linux/psonoci |
| AArch64 | Linux | https://get.psono.com/psono/psono-ci/aarch64-linux/psonoci |

Depending on your flavor your can install psonoci like shown here:

```bash
curl https://get.psono.com/psono/psono-ci/x86_64-linux/psonoci --output psonoci && chmod +x psonoci
```
